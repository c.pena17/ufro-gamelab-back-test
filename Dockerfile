FROM eclipse-temurin:17-jdk-alpine as build
WORKDIR /workspace/app
# COPY mvnw .
# COPY .mvn .mvn
# COPY pom.xml .
# COPY src src
# RUN ./mvnw dependency:go-offline
# RUN ./mvnw clean package -DskipTests

COPY target/*.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]