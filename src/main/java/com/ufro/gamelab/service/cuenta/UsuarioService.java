package com.ufro.gamelab.service.cuenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufro.gamelab.model.Usuario;
import com.ufro.gamelab.repository.cuenta.UsuarioRepository;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario saveUsuario(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Usuario findByNombreUsuario(String nombreUsuario) {
        return usuarioRepository.findByNombreUsuario(nombreUsuario);
    }

    public Usuario findByEmail(String email) {
        return usuarioRepository.findByEmail(email);
    }

    public Usuario buscarPorTexto(String texto) {
        Usuario usuario = findByNombreUsuario(texto);
        if (usuario != null) {
            return usuario;
        } else {
            return findByEmail(texto);
        }
    }
}