package com.ufro.gamelab.service.cuenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.ufro.gamelab.model.Usuario;
import com.ufro.gamelab.model.cuenta.ConfirmacionToken;
import com.ufro.gamelab.model.utils.EstadoEnLinea;
import com.ufro.gamelab.repository.cuenta.ConfirmacionCuentaRepository;
import com.ufro.gamelab.repository.cuenta.RegistroUsuarioRepository;

import com.ufro.gamelab.dto.cuenta.UsuarioRegistroDto;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class RegistroUsuarioService {
    @Autowired
    private RegistroUsuarioRepository registroUsuarioRepository;

    @Autowired
    private ConfirmacionCuentaRepository confirmacionCuentaRepository;

    @Autowired
    private JavaMailSender mailSender;

    public String registrarUsuario(UsuarioRegistroDto usuarioDTO) {
        Usuario usuario = new Usuario();
        usuario.setNombreUsuario(usuarioDTO.getNombreUsuario());
        usuario.setContrasenia(usuarioDTO.getContrasenia());
        usuario.setEstadoEnLinea(EstadoEnLinea.INACTIVO); // Estado inicial
        usuario.setFechaUltimaConexion(LocalDateTime.now()); // Inicialmente
        usuario.setEstadoCuenta(false);
        usuario.setEmail(usuarioDTO.getEmail());

        registroUsuarioRepository.save(usuario);

        String token = UUID.randomUUID().toString();
        ConfirmacionToken confirmacionToken = new ConfirmacionToken();
        confirmacionToken.setToken(token);
        confirmacionToken.setUsuario(usuario);
        confirmacionToken.setFechaCreacion(LocalDateTime.now());
        confirmacionToken.setFechaExpiracion(LocalDateTime.now().plusHours(24)); // Token válido por 24 horas
        confirmacionCuentaRepository.save(confirmacionToken);

        //enviarEmailConfirmacion(usuarioDTO.getEmail(), token);
        return token;
    }

    private void enviarEmailConfirmacion(String email, String token) { //arreglar
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Confirma tu cuenta");
        message.setText("Para confirmar tu cuenta, haz clic en el siguiente enlace: "
                + "http://localhost:5500/confirmar?token=" + token);
        mailSender.send(message);
    }

    public boolean confirmarUsuario(String token, String nuevaContrasenia) {
        ConfirmacionToken confirmacionToken = confirmacionCuentaRepository.findByToken(token);
        if (confirmacionToken != null && confirmacionToken.getFechaExpiracion().isAfter(LocalDateTime.now())) {
            Usuario usuario = confirmacionToken.getUsuario();
            usuario.setContrasenia(nuevaContrasenia);
            registroUsuarioRepository.save(usuario);
            confirmacionCuentaRepository.delete(confirmacionToken); 
            return true;
        }
        return false;
    }
}
