package com.ufro.gamelab.service.cuenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ufro.gamelab.dto.cuenta.UserProfilDto;
import com.ufro.gamelab.model.PerfilUsuario;
import com.ufro.gamelab.model.Usuario;
import com.ufro.gamelab.repository.cuenta.ProfileRepository;
import com.ufro.gamelab.repository.cuenta.UsuarioRepository;

@Service
public class VerPerfilService {

    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    public UserProfilDto getProfileUser(String nombreUsuario) {
        Usuario usuario = usuarioRepository.findByNombreUsuario(nombreUsuario);
        if (usuario == null) {
            return null; // Manejar el caso cuando el usuario no es encontrado
        }
        PerfilUsuario perfilUsuario = profileRepository.findByUsuarioUsuarioId(usuario.getUsuarioId());
        if (perfilUsuario == null) {
            return null; // Manejar el caso cuando el perfil del usuario no es encontrado
        }
        return new UserProfilDto(perfilUsuario.getImagen(), usuario.getNombreUsuario(), usuario.getEmail(), perfilUsuario.getFechaNacimiento(), perfilUsuario.getDescripcion());
    }
}
