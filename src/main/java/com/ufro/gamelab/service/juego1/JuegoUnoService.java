package com.ufro.gamelab.service.juego1;

import com.ufro.gamelab.dto.juego1.PuntajeUsuarioDTO;
import org.springframework.stereotype.Service;

import com.ufro.gamelab.dto.juego1.PuntajeDTO;
import com.ufro.gamelab.model.CuentaJugador;
import com.ufro.gamelab.model.Juego;
import com.ufro.gamelab.model.Puntaje;
import com.ufro.gamelab.model.Usuario;
import com.ufro.gamelab.repository.cuenta.CuentaJugadorRepository;
import com.ufro.gamelab.repository.cuenta.UsuarioRepository;
import com.ufro.gamelab.repository.juego1.JuegoUnoRepository;
import java.util.List;
import jakarta.persistence.EntityNotFoundException;

import com.ufro.gamelab.repository.juego1.JuegoRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class JuegoUnoService {

    private JuegoUnoRepository juegoUnoRepository;

    private CuentaJugadorRepository cuentaJugadorRepository;

    private JuegoRepository juegoRepository;

    private UsuarioRepository usuarioRepository;

    public JuegoUnoService(JuegoUnoRepository juegoUnoRepository, CuentaJugadorRepository cuentaJugadorRepository,
            JuegoRepository juegoRepository, UsuarioRepository usuarioRepository) {
        this.juegoRepository = juegoRepository;
        this.cuentaJugadorRepository = cuentaJugadorRepository;
        this.juegoUnoRepository = juegoUnoRepository;
        this.usuarioRepository = usuarioRepository;
    }

    public Puntaje guardarPuntaje(PuntajeDTO puntajeDTO) {
        int juegoId = 1;

        Usuario user = usuarioRepository.findById(puntajeDTO.getUserId())
                .orElseThrow(() -> new EntityNotFoundException("Usuario no encontrado"));

        Juego juego = juegoRepository.findById(juegoId)
                .orElseThrow(() -> new EntityNotFoundException("Juego no encontrado"));

        CuentaJugador cuentaJugador = cuentaJugadorRepository.findByUsuario(user);
        if (cuentaJugador == null) {
            throw new EntityNotFoundException("Cuenta de jugador no encontrada");
        }
      
        Puntaje puntaje = juegoUnoRepository.findByCuentaJugadorAndJuego(cuentaJugador, juego);


        if (puntaje != null) {
            if (puntaje.getValor() < puntajeDTO.getPuntaje()) {
                puntaje.setValor(puntajeDTO.getPuntaje());
            } 
        } else {
            puntaje = new Puntaje();
            puntaje.setCuentaJugador(cuentaJugador);
            puntaje.setJuego(juego);
            puntaje.setValor(puntajeDTO.getPuntaje());
        }

        return juegoUnoRepository.save(puntaje);
    }

    public List<PuntajeUsuarioDTO> getPuntajes() {
        List<PuntajeUsuarioDTO> puntajesFinal = new ArrayList<>();
        List<Puntaje> puntajesTemp = juegoUnoRepository.findAllByOrderByValorDesc();

        for (Puntaje puntaje : puntajesTemp) {

            CuentaJugador jugador = cuentaJugadorRepository.findById(puntaje.getCuentaJugador().getJugadorId()).get();
            PuntajeUsuarioDTO puntajeUsuario = new PuntajeUsuarioDTO(jugador.getUsuario().getNombreUsuario(),
                    puntaje.getValor());

            puntajesFinal.add(puntajeUsuario);
        }
        return puntajesFinal;
    }

}
