package com.ufro.gamelab.dto.cuenta;
import lombok.Data;

@Data
public class UsuarioRegistroDto {
    private String nombreUsuario;
    private String contrasenia;
    private String email;
    private String rol;
}