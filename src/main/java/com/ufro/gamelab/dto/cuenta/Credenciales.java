package com.ufro.gamelab.dto.cuenta;

public class Credenciales {
    private String credencial;
    private String contrasenia;

    public Credenciales() {}

    public Credenciales(String credencial, String contrasenia) {
        this.credencial = credencial;
        this.contrasenia = contrasenia;
    }

    public String getCredencial(){
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }

    public String getContrasenia(){
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
}