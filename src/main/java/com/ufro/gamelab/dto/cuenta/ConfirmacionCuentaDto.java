package com.ufro.gamelab.dto.cuenta;

import lombok.Data;

@Data
public class ConfirmacionCuentaDto {
    private String token;
    private String nuevaContrasenia;
}
