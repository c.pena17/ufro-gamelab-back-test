package com.ufro.gamelab.dto.cuenta;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Date;

@Data
@AllArgsConstructor
public class UserProfilDto {
    private byte[] imagen;
    private String nombreUsuario;
    private String correo;
    private Date fechaNacimiento;
    private String descripcion;
}
