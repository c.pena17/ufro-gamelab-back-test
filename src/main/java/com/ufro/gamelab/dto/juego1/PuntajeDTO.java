package com.ufro.gamelab.dto.juego1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PuntajeDTO {
    private int userId;
    private int puntaje;
}
