package com.ufro.gamelab.dto.juego1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PuntajeUsuarioDTO {

    String usuario;
    int puntaje;

}
