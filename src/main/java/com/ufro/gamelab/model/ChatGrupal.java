package com.ufro.gamelab.model;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name="chat_grupal")
public class ChatGrupal {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "chat_grupal_id")
    private int chatGrupalId;

    @Column(name= "nombre")
    private String nombre;

    @Column(name= "descripcion")
    private String descripcion;

    @Column(name = "fecha_creacion", nullable = false, columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "creador_id")
    private Usuario creador;

}
