package com.ufro.gamelab.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinColumns;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "puntaje")
public class Puntaje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "puntaje_id")
    private long puntajeId;

    @ManyToOne
    @JoinColumn(name = "juego_id", nullable = false)
    @JsonBackReference
    private Juego juego;

    @ManyToOne
    @JoinColumn(name = "jugador_id", nullable = false)
    @JsonBackReference
    private CuentaJugador cuentaJugador;

    @ManyToOne
    @JoinColumn(name = "nivel", nullable = true)
    private Nivel nivel;

    @Column(name = "valor", nullable = false)
    private int valor;
}
