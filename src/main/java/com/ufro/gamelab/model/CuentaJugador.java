package com.ufro.gamelab.model;

import java.util.List;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cuenta_jugador")
public class CuentaJugador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "jugador_id", nullable = false)
    private int jugadorId;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)

    @JoinColumn(name = "usuario", nullable = false)
    @JsonBackReference
    private Usuario usuario;

    @OneToMany(mappedBy = "cuentaJugador")
    @Column(name = "puntajes", nullable = true)
    @JsonManagedReference
    @ToString.Exclude
    private List<Puntaje> puntajes;

    @ManyToMany
    @JoinTable(name = "jugador_juego", joinColumns = @JoinColumn(name = "jugador_id"), inverseJoinColumns = @JoinColumn(name = "juego_id"))
    @JsonManagedReference
    // @ToString.Exclude
    private List<Juego> juegos;

}
