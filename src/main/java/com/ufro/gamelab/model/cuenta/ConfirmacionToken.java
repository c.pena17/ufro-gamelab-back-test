package com.ufro.gamelab.model.cuenta;

import java.time.LocalDateTime;

import com.ufro.gamelab.model.Usuario;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class ConfirmacionToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String token;

    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "usuario_id")
    private Usuario usuario;

    @Column(nullable = false)
    private LocalDateTime fechaCreacion;

    @Column(nullable = false)
    private LocalDateTime fechaExpiracion;
}
