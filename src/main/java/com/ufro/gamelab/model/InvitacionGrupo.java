package com.ufro.gamelab.model;


import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Data
@Entity
@Table(name="invitacion_grupo")
public class InvitacionGrupo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "invitacion_grupo_id")
    private int invitacionGrupoId;

    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @Column(name = "aceptada", columnDefinition = "Boolean default FALSE")
    private Boolean aceptada;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "emisor_id", nullable = false)
    private Usuario emisor;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "receptor_id", nullable = false)
    private Usuario receptor;

    @JoinColumn(name = "notificacion_id", referencedColumnName = "notificacion_id")
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Notificacion notificacion;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "chat_grupal_id", nullable = false)
    private ChatGrupal chatGrupal;

}
