package com.ufro.gamelab.model;

import java.time.LocalDateTime;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "recibo")
@Getter
@Setter
public class Recibo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recibo_id",nullable = false)
    private long id;

    @ManyToOne
    @JoinColumn(name = "orden_compra_id", referencedColumnName = "orden_compra_id", nullable = false)
    private OrdenCompra ordenCompra;

    @Column(name = "precio_total", nullable = false)
    private long precioTotal;

    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @Column(name = "detalle",columnDefinition = "TEXT", nullable = false)
    private String detalle;
}
