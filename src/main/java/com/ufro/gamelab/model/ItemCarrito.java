package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="item_carrito")
@Getter
@Setter
public class ItemCarrito {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_carrito_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "carrito_id", referencedColumnName = "carrito_id", nullable = false)
    private Carrito carrito;

    @ManyToOne
    @JoinColumn(name = "juego_id", referencedColumnName = "juego_id")
    private Juego juego;
}
