package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.UsuarioAniadeUsuarioId;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(UsuarioAniadeUsuarioId.class)
@Table(name = "usuario_aniade_usuario")
public class UsuarioAniadeUsuario {

    @Column(name = "usuario_aniade_usuario_id", nullable = false, columnDefinition = "serial")
    private long usuarioAniadeUsuario;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "usuario_uno_id", nullable = false)
    private Usuario usuarioUno;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "usuario_dos_id", nullable = false)
    private Usuario usuarioDos;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;
}
