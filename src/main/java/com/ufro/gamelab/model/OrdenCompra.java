package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.ufro.gamelab.model.utils.EstadoOrdenDeCompra;

@Entity
@Table(name="orden_compra")
@Getter
@Setter
public class OrdenCompra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orden_compra_id")
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado", nullable = false)
    private EstadoOrdenDeCompra estado;

    @Column(name = "precio_total", nullable = false)
    private BigDecimal precioTotal;

    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP", nullable = false)
    private LocalDateTime fecha;

    @OneToOne
    @JoinColumn(name = "carrito_id", referencedColumnName = "carrito_id", nullable = false)
    private Carrito carrito;

    @OneToMany(mappedBy = "ordenCompra")
    private List<Recibo> recibos;
}
