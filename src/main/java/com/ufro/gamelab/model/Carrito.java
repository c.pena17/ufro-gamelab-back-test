package com.ufro.gamelab.model;

import java.math.BigDecimal;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="carrito")
@Getter
@Setter
public class Carrito {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "carrito_id")
    private long id;

    @Column(name = "total", nullable = false, columnDefinition = "Numeric")
    private BigDecimal total;

    @OneToOne(mappedBy = "carrito")
    private OrdenCompra ordenCompra;

    @ManyToOne
    @JoinColumn(name = "jugador_id", referencedColumnName = "jugador_id",nullable = false)
    private CuentaJugador cuentaJugador;

    @OneToMany(mappedBy = "carrito")
    private List<ItemCarrito> items;

}
