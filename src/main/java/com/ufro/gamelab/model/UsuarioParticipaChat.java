package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.UsuarioParticipaChatId;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(UsuarioParticipaChatId.class)
@Table(name = "usuario_participa_chat")
public class UsuarioParticipaChat {

    @Column(name = "usuario_participa_chat_id", nullable = false, columnDefinition = "serial")
    private long usuarioParticipaChat;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "chat_grupal_id", nullable = false)
    private ChatGrupal chatGrupal;
}
