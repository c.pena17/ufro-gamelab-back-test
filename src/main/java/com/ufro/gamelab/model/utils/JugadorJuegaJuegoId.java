package com.ufro.gamelab.model.utils;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JugadorJuegaJuegoId implements Serializable {
    private int cuentaJugador;
    private int juego;
}
