package com.ufro.gamelab.model.utils;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JugadorPoseeJuegoId implements Serializable{
    private int cuentaJugador;
    private int juego;
}
