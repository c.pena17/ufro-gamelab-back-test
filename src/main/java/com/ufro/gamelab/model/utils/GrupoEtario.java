package com.ufro.gamelab.model.utils;

// basado en: Consejo de Calificación Cinematográfica para videojuegos
public enum GrupoEtario {
    ER,
    TE,
    OCHOMAS,
    CATORCEMAS,
    DIECIOCHOMAS
}
