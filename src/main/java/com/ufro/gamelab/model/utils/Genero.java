package com.ufro.gamelab.model.utils;

public enum Genero {
    COMBATE,
    LABERINTO,
    DEPORTES,
    CARRERA,
    AVENTURA,
    ROL,
    GUERRA,
    RITMO,
    COMPETITIVO,
    DISPAROS,
    HORROR
}
