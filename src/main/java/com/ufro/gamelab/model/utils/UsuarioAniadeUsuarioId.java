package com.ufro.gamelab.model.utils;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsuarioAniadeUsuarioId implements Serializable {
    private int usuarioUno;
    private int usuarioDos;
}
