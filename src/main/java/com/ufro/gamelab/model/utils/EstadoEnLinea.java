package com.ufro.gamelab.model.utils;

public enum EstadoEnLinea {
    ACTIVO,
    INACTIVO,
    OCUPADO
}
