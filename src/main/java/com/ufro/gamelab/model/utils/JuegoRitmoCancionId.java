package com.ufro.gamelab.model.utils;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JuegoRitmoCancionId implements Serializable {
    private long juegoRitmo;
    private long cancion;
}
