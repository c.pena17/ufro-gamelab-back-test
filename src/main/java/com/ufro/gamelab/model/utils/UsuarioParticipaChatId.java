package com.ufro.gamelab.model.utils;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioParticipaChatId implements Serializable {
    private int usuario;
    private long chatGrupal;
}
