package com.ufro.gamelab.model.utils;

public enum EstadoNotificacion {
    NO_LEIDA,
    LEIDA
}
