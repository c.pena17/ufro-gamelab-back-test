package com.ufro.gamelab.model.utils;

public enum TipoNotificacion {
    SOLICITUD_AMISTAD,
    MENSAJE_RECIBIDO,
    INVITACION_GRUPO
}
