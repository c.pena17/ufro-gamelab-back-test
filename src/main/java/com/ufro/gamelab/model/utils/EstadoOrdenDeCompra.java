package com.ufro.gamelab.model.utils;

public enum EstadoOrdenDeCompra {
    PENDIENTE,
    PAGADA,
    CANCELADA,
    COMPLETADA
}
