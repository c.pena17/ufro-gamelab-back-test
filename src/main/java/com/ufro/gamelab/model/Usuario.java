package com.ufro.gamelab.model;

import java.time.LocalDateTime;

import com.ufro.gamelab.model.utils.EstadoEnLinea;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuario_id", nullable = false)
    private int usuarioId;

    @Column(name = "nombre_usuario", nullable = false, unique = true, length = 50)
    private String nombreUsuario;

    @Column(name = "contrasenia", nullable = false, length = 255)
    private String contrasenia;

    @Column(name = "estado_cuenta", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean estadoCuenta;

    @Column(name = "fecha_ultima_conexion", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime fechaUltimaConexion = LocalDateTime.now();

    @Enumerated(EnumType.STRING)
    @Column(name = "estado_en_linea", nullable = false, length = 255)
    private EstadoEnLinea estadoEnLinea;

    @OneToOne(mappedBy = "usuario")
    @PrimaryKeyJoinColumn
    private PerfilUsuario perfilUsuario;

    @Column(name = "email", nullable = false, unique = true, length = 500)
    private String email;


    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
}
