package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.EstadoNotificacion;
import com.ufro.gamelab.model.utils.TipoNotificacion;

@Data
@Entity
@Table(name="notificacion")
public class Notificacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "notificacion_id")
    private int notificacionId;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "receptor_id", nullable = true)
    private Usuario receptor;

    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @Column(name= "contenido")
    private String contendio;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_notificacion")
    private TipoNotificacion tipoNotificacion;

    @Enumerated(EnumType.STRING)
    @Column(name= "estado_notificacion")
    private EstadoNotificacion estadoNotificacion;

    @ManyToOne
    @JoinColumn(name = "solicitud_amistad_id", nullable = true)
    private SolicitudAmistad solicitudAmistad;

    @ManyToOne
    @JoinColumn(name = "chat_id", nullable = true)
    private Chat chat;

    @ManyToOne
    @JoinColumn(name = "invitacion_grupo_id", nullable = true)
    private InvitacionGrupo invitacionGrupo;
}
