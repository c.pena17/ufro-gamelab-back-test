package com.ufro.gamelab.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinColumns;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "juego_plataforma")
public class JuegoPlataforma {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "juego_plataforma_id")
    private long juegoPlataformaId;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumns({
            @JoinColumn(name = "juego_id", referencedColumnName = "juego_id"),
            @JoinColumn(name = "jugador_id", referencedColumnName = "jugador_id")
    })
    private JugadorJuegaJuego jugadorJuegaJuego;

    @Column(name = "puntaje", nullable = false)
    private int puntaje;
}
