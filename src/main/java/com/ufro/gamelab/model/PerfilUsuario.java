package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "perfil_usuario")
public class PerfilUsuario {

    @Id
    @Column(name = "usuario_id")
    private int id;

    @OneToOne
    @MapsId
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    @Column(name = "imagen", nullable = true, length = 255)
    private byte[] imagen;

    @Column(name = "descripcion", nullable = true, length = 255)
    private String descripcion;

    @Column(name = "fecha_nacimiento", nullable = true)
    private Date fechaNacimiento;
}
