package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.JugadorPoseeJuegoId;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(JugadorPoseeJuegoId.class)
@Table(name = "jugador_posee_juego")
public class JugadorPoseeJuego {

    @Column(name = "jugador_posee_juego_id", nullable = false, columnDefinition = "serial")
    private long jugadorPoseeJuegoId;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "jugador_id", nullable = false)
    private CuentaJugador cuentaJugador;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "juego_id", nullable = false)
    private Juego juego;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_adquirido", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha_adquirido;

    @Column(name = "favorito", columnDefinition = "Boolean default false")
    private Boolean favorito;

    @Column(name = "total_tiempo_jugado", columnDefinition = "INTERVAL")
    private LocalDateTime total_tiempo_jugado;

}
