package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.EstadoSolicitudAmistad;

@Data
@Entity
@Table(name="solicitud_amistad")
public class SolicitudAmistad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "solicitud_amistad_id")
    private int soliciutudAmistadId;

    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @Enumerated(EnumType.STRING)
    @Column(name= "estado_solicitud_amistad")
    private EstadoSolicitudAmistad estadoSolicitudAmistad;

    @JoinColumn(name = "notificacion_id", referencedColumnName = "notificacion_id")
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Notificacion notificacion;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "emisor_id", nullable = false)
    private Usuario emisor;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "receptor_id", nullable = false)
    private Usuario receptor;

}
