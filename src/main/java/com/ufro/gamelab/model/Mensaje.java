package com.ufro.gamelab.model;


import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Data
@Entity
@Table(name="mensaje")
public class Mensaje {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "mensaje_id")
    private int mensajeId;

    @Column(name= "contenido")
    private String contenido;

    @Column(name= "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @Column(name= "visto")
    private Boolean visto;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "emisor_id", nullable = false)
    private Usuario emisor;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "chat_id", nullable = false)
    private Chat chat;
}
