package com.ufro.gamelab.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.JugadorRealizaReseniaId;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(JugadorRealizaReseniaId.class)
@Table(name = "jugador_realiza_resenia")
public class JugadorRealizaResenia {

    @Column(name = "jugador_realiza-renia_id", nullable = false, columnDefinition = "serial")
    private long jugadorRealizaReseniaId;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "jugador_id", nullable = false)
    private CuentaJugador cuentaJugador;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "juego_id", nullable = false)
    private Juego juego;

    @Column(name = "positiva")
    private boolean positiva;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fecha;

    @Column(name = "comentario", nullable = false)
    private String comentario;
}
