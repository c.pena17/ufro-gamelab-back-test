package com.ufro.gamelab.model;


import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Data
@Entity
@Table(name="chat")
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "chat_id")
    private int chatId;

    @Column(name = "fecha_creacion", columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fechaCreacion;

    @Column(name = "silenciado", columnDefinition = "Boolean default FALSE")
    private Boolean silenciado;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "creador_id", nullable = false)
    private Usuario creador;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "participante_id", nullable = false)
    private Usuario participante;


}
