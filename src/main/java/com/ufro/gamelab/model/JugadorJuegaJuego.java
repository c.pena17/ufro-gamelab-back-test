package com.ufro.gamelab.model;

import java.time.LocalDateTime;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.ufro.gamelab.model.utils.JugadorJuegaJuegoId;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "jugador_juega_juego")
@IdClass(JugadorJuegaJuegoId.class)
public class JugadorJuegaJuego {

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "jugador_id", nullable = false)
    private CuentaJugador cuentaJugador;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "juego_id", nullable = false)
    private Juego juego;

    @Column(name = "jugador_juega_juego_id", nullable = false, columnDefinition = "serial")
    private long jugadorJuegaJuegoId;

    @Column(name = "fecha_inicio_juego", nullable = true, columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fechaInicioJuego;

    @Column(name = "tiempo_total_jugado", nullable = true, columnDefinition = "INTERVAL")
    private LocalDateTime tiempoTotalJugado;

    // @OneToOne(mappedBy = "puntaje")
    // private Puntaje puntaje;
}
