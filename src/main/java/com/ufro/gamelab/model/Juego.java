package com.ufro.gamelab.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ufro.gamelab.model.utils.Genero;
import com.ufro.gamelab.model.utils.GrupoEtario;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "juego")
public class Juego {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "juego_id")
    private int juegoId;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "desarrollador_id", nullable = false)
    @JsonBackReference
    private CuentaDesarrollador cuentaDesarrollador;

    @Column(name = "path", nullable = true, length = 255)
    private String path;

    @Column(name = "nombre_juego", nullable = false, length = 255)
    private String nombreJuego;

    @Column(name = "descripcion_juego", nullable = true, columnDefinition = "TEXT")
    private String descripcionJuego;

    @Column(name = "imagen_portada", nullable = true, length = 255)
    private String imagenPortada;

    @Column(name = "imagen_banner", nullable = true, length = 255)
    private String imagenBanner;

    @Column(name = "video", nullable = true, length = 255)
    private String video;

    @Column(name = "precio", nullable = false, columnDefinition = "NUMERIC")
    private BigDecimal precio;

    @Column(name = "fecha_lanzamiento", nullable = true, columnDefinition = "TIMESTAMP")
    private LocalDateTime fechaLanzamiento;

    @Column(name = "fecha_creacion", nullable = false, columnDefinition = "Timestamp default CURRENT_TIMESTAMP")
    private LocalDateTime fechaCreacion;

    @Enumerated(EnumType.STRING)
    @Column(name = "genero", nullable = false, length = 255)
    private Genero genero;

    @Enumerated(EnumType.STRING)
    @Column(name = "grupo_etario", nullable = false, length = 255)
    private GrupoEtario grupoEtario;

    @OneToMany(mappedBy = "juego", cascade = CascadeType.ALL)
    @JsonBackReference
    @ToString.Exclude
    private List<Puntaje> puntajes;

    @ManyToMany(mappedBy = "juegos")
    private List<CuentaJugador> jugadores;
}
