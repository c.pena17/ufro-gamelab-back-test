package com.ufro.gamelab.repository.juego1;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ufro.gamelab.model.CuentaJugador;
import com.ufro.gamelab.model.Juego;
import com.ufro.gamelab.model.Puntaje;

import java.util.List;

@Repository
public interface JuegoUnoRepository extends JpaRepository<Puntaje, Integer>{


    List<Puntaje> findAllByOrderByValorDesc();

    List<Puntaje> findByCuentaJugador(CuentaJugador cuentaJugador);

    Puntaje findByCuentaJugadorAndJuego(CuentaJugador cuentaJugador, Juego juego);
} 