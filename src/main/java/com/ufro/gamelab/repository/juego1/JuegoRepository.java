package com.ufro.gamelab.repository.juego1;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ufro.gamelab.model.Juego;

@Repository
public interface JuegoRepository extends JpaRepository<Juego, Integer>{
    
}
