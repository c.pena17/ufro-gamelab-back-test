package com.ufro.gamelab.repository.cuenta;
import com.ufro.gamelab.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface  RegistroUsuarioRepository extends JpaRepository<Usuario, Integer>  {
   
}
