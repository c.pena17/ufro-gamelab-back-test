package com.ufro.gamelab.repository.cuenta;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ufro.gamelab.model.CuentaJugador;
import com.ufro.gamelab.model.Usuario;

@Repository
public interface CuentaJugadorRepository extends JpaRepository<CuentaJugador, Integer>{

    CuentaJugador findByUsuario(Usuario usuario);

}
