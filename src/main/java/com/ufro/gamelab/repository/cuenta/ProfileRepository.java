package com.ufro.gamelab.repository.cuenta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ufro.gamelab.model.PerfilUsuario;

@Repository
public interface ProfileRepository extends JpaRepository<PerfilUsuario, Integer> {
    PerfilUsuario findByUsuarioUsuarioId(int usuarioId);
}
