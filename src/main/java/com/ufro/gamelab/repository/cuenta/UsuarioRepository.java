package com.ufro.gamelab.repository.cuenta;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ufro.gamelab.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    Usuario findByNombreUsuario(String nombreUsuario);
    Usuario findByEmail(String email);
}
