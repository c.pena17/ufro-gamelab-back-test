package com.ufro.gamelab.repository.cuenta;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ufro.gamelab.model.cuenta.ConfirmacionToken;

public interface ConfirmacionCuentaRepository extends JpaRepository<ConfirmacionToken, Long>{
    ConfirmacionToken findByToken(String token);
}
