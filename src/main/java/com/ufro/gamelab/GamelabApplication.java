package com.ufro.gamelab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamelabApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamelabApplication.class, args);
	}

}
