package com.ufro.gamelab.controller.cuenta;

import com.ufro.gamelab.dto.cuenta.ConfirmacionCuentaDto;
import com.ufro.gamelab.service.cuenta.RegistroUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/prueba-avance/cuenta")
public class ConfirmacionCuentaController {
    @Autowired
    private RegistroUsuarioService registroUsuarioService;

    @PostMapping("/confirmar")
    public String confirmarCuenta(@RequestBody ConfirmacionCuentaDto confirmacionDTO) {
        boolean confirmado = registroUsuarioService.confirmarUsuario(confirmacionDTO.getToken(), confirmacionDTO.getNuevaContrasenia());
        if (confirmado) {
            return "Cuenta confirmada con éxito.";
        } else {
            return "El enlace de confirmación es inválido o ha expirado.";
        }
    }
}
