package com.ufro.gamelab.controller.cuenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ufro.gamelab.dto.cuenta.Credenciales;
import com.ufro.gamelab.model.Usuario;
import com.ufro.gamelab.service.cuenta.CorreoService;
import com.ufro.gamelab.service.cuenta.UsuarioService;

@RestController
@RequestMapping("/user")
public class CambiarPasswordController {
    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private CorreoService correoService;

    @GetMapping("/find-user/{texto}")
    public ResponseEntity<Usuario> buscarUsuario(@PathVariable String texto) {
        Usuario usuario = usuarioService.buscarPorTexto(texto);
        if (usuario != null) {
            return ResponseEntity.ok(usuario);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/reset-password")
    public ResponseEntity<?> cambiarContrasenia(@RequestBody Credenciales credenciales) {
        String credencial = credenciales.getCredencial();
        String nuevaContrasenia = credenciales.getContrasenia();

        Usuario usuario = usuarioService.buscarPorTexto(credencial);

        if (usuario == null) {
            return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
        }

        if (usuario.getContrasenia().equals(nuevaContrasenia)) {
            return new ResponseEntity<>("La contraseña nueva no puede ser igual a la anterior.", HttpStatus.BAD_REQUEST);
        }

        usuario.setContrasenia(nuevaContrasenia);
        usuarioService.saveUsuario(usuario);

        return new ResponseEntity<>("Contraseña cambiada exitosamente.", HttpStatus.OK);
    }

    @PostMapping("/request-reset")
    public ResponseEntity<?> enviarCorreo(@RequestBody Credenciales sendEmailRequest) {
        String credencial = sendEmailRequest.getCredencial();

        Usuario usuario = usuarioService.buscarPorTexto(credencial);

        if (usuario == null) {
            return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
        }

        String email = usuario.getEmail();

        try {
            correoService.sendSimpleEmail(email, "Solicitud de Cambio de Contraseña", "Hola " + usuario.getNombreUsuario() + ", te enviamos el enlace para cambiar tu contraseña.");
            return new ResponseEntity<>("Correo enviado exitosamente.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error al enviar el correo: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}