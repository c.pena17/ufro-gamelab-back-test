package com.ufro.gamelab.controller.cuenta;

import com.ufro.gamelab.dto.cuenta.UsuarioRegistroDto;
import com.ufro.gamelab.service.cuenta.RegistroUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/prueba-avance/cuenta")
public class RegistroUsuarioController {
    @Autowired
    private RegistroUsuarioService registroUsuarioService;

    @PostMapping("/registrar")
    public String registrarUsuario(@RequestBody UsuarioRegistroDto usuarioDTO) {
        String token= registroUsuarioService.registrarUsuario(usuarioDTO);
        return "Registro exitoso. Por favor, revisa tu correo para confirmar tu cuenta." + token;
    }
}
