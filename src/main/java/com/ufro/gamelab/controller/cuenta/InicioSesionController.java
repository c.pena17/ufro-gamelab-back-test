package com.ufro.gamelab.controller.cuenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ufro.gamelab.dto.cuenta.Credenciales;
import com.ufro.gamelab.model.Usuario;
import com.ufro.gamelab.service.cuenta.UsuarioService;

@RestController
@RequestMapping("/user")
public class InicioSesionController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/crear")
    public ResponseEntity<?> createUsuario(@RequestBody Usuario usuario) {
        try {
            Usuario savedUsuario = usuarioService.saveUsuario(usuario);
            return new ResponseEntity<>(savedUsuario, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Error al crear usuario: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> iniciarSesion(@RequestBody Credenciales loginRequest) {
        String credencial = loginRequest.getCredencial();
        String contrasenia = loginRequest.getContrasenia();

        Usuario usuario = usuarioService.buscarPorTexto(credencial);

        if (usuario != null && usuario.getContrasenia().equals(contrasenia)) {
            return new ResponseEntity<>("Sesion iniciada", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Usuario no encontrado. Por favor, verifica tu nombre de usuario/correo y contraseña.", HttpStatus.UNAUTHORIZED);
        }
    }
}