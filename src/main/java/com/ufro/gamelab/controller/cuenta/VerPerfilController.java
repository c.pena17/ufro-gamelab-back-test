package com.ufro.gamelab.controller.cuenta;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ufro.gamelab.dto.cuenta.UserProfilDto;
import com.ufro.gamelab.service.cuenta.VerPerfilService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("/perfil")
public class VerPerfilController {

    @Autowired
    private VerPerfilService VerPerfilService;

    @GetMapping("")
    public UserProfilDto showProfile() {
        return this.VerPerfilService.getProfileUser("usuario1");
    }
    

}
