package com.ufro.gamelab.controller.juego1;

import java.util.List;

import com.ufro.gamelab.dto.juego1.PuntajeUsuarioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ufro.gamelab.dto.juego1.PuntajeDTO;
import com.ufro.gamelab.model.Puntaje;
import com.ufro.gamelab.service.juego1.JuegoUnoService;

@RestController
@RequestMapping("/api/juego1")
public class JuegoUnoController {

    @Autowired
    private JuegoUnoService juegoUnoService;

    public JuegoUnoController(JuegoUnoService juegoUnoService) {
        this.juegoUnoService = juegoUnoService;
    }


    @GetMapping
    public List<PuntajeUsuarioDTO> getAllScores() {
        return juegoUnoService.getPuntajes();
    }



    @PostMapping
    public Puntaje createScore(@RequestBody PuntajeDTO puntaje) {
        return juegoUnoService.guardarPuntaje(puntaje);

    }

}
